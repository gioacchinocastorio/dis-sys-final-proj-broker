
# Broker Microservice #

Distributed Systems final project.

### How do I get set up? ###

Make sure to have installed the following dependencies:
* Java 1.8
* Gradle (_optional_)

### Run commands ###

At least in Unix-like environments, it is sufficient to run the MongoDB server
and then to type on terminal the following commands
~~~~~
In the project path

Mac OSX: $ ./gradlew run
Linux: $ sh ./gradlew run
Windows: > gradlew run
~~~~~

Make sure that the listening port is free for the web service and that the addresses and ports of the other services are correct. You may change it in the configuration file
~~~~~
File: configs/config.ini

; Rest server configuration
[Spark]
port = 34567

; Rest User store and Procedure Store configuration
[Rest]
address_users = localhost
port_users = 12345
address_procedures = localhost
port_procedures = 23456
user_path = /api/user/
procedure_path = /api/procedure/

...

; Remote procedure configuration
[SOAP]
wsdl = http://localhost:9000/PasswordService/server?wsdl
service_namespace = http://soapservice/
service_name = PasswordGenerationServer
~~~~~
