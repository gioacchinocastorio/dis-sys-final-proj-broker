package utils.requestintegritycheck;

/**
 * Class to check if the requests are well formed
 */
public interface IntegrityChecker {

    /**
     * Check if the new password request is well formed
     *
     * @param jsonRequest json serializing the new password input request
     * @return true if well formed
     */
    boolean isPasswordCreationRequestWellFormed(String jsonRequest);
}
