package utils.rest;

import business.messages.ServicePageRetrievalMessage;
import business.pojo.Procedure;
import utils.customexceptions.AuthException;
import utils.customexceptions.NoProcedureException;
import utils.customexceptions.NoUserException;
import utils.customexceptions.ServiceUnavailableException;

import java.util.List;

/**
 * Procedure store REST point-of-access
 */
public interface ProcedureStoreRestClient {

    /**
     * Create a new procedure in the store
     *
     * @param procedure parameters for the procedure
     * @param authToken auth token of the user
     * @throws ServiceUnavailableException Thrown if one of the components of the back-end is unavailable
     * @throws AuthException               Thrown if the specified userID and authToken are not valid or consistent
     */
    void createProcedure(Procedure procedure, String authToken) throws ServiceUnavailableException, AuthException;

    /**
     * Update a procedure in the store
     *
     * @param procedure new parameters for the procedure
     * @param authToken auth token of the user
     * @throws ServiceUnavailableException
     * @throws AuthException               * @throws ServiceUnavailableException Thrown if one of the components of the back-end is unavailable
     * @throws AuthException               Thrown if the specified userID and authToken are not valid or consistent
     */
    void updateProcedure(Procedure procedure, String authToken) throws ServiceUnavailableException, AuthException;

    /**
     * Retrieve a procedure from the store
     *
     * @param ownerID     id of the password owner in the user store
     * @param procedureID id of the procedure in the store
     * @param authToken   auth token of the user
     * @return retrieved procedure
     * @throws NoProcedureException        Thrown if the specified procedure does not exist
     * @throws ServiceUnavailableException Thrown if one of the components of the back-end is unavailable
     * @throws AuthException               Thrown if the specified userID and authToken are not valid or consistent
     */
    Procedure getProcedure(String ownerID, String procedureID, String authToken) throws NoProcedureException, ServiceUnavailableException, AuthException;

    /**
     * Delete a procedure in the store
     *
     * @param ownerID     id of the password owner in the user store
     * @param procedureID id of the procedure in the store
     * @param authToken   auth token of the user
     * @throws ServiceUnavailableException Thrown if one of the components of the back-end is unavailable
     * @throws AuthException               Thrown if the specified userID and authToken are not valid or consistent
     */
    void deleteProcedure(String ownerID, String procedureID, String authToken) throws ServiceUnavailableException, AuthException;

    /**
     * Retrieve procedure info (page)
     *
     * @param ownerID    id of the password owner in the user store
     * @param pageNumber number of the page in the store
     * @param pageRange  range in the page
     * @param authToken  auth token of the user
     * @return retrieved page
     * @throws ServiceUnavailableException Thrown if one of the components of the back-end is unavailable
     * @throws AuthException               Thrown if the specified userID and authToken are not valid or consistent
     */
    List<ServicePageRetrievalMessage> getPageProcedures(String ownerID, int pageNumber, int pageRange, String authToken) throws ServiceUnavailableException, AuthException;

}
