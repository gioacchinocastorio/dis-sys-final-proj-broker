package utils.rest;

import business.pojo.User;
import utils.customexceptions.AuthException;
import utils.customexceptions.NoProcedureException;
import utils.customexceptions.NoUserException;
import utils.customexceptions.ServiceUnavailableException;

/**
 * User store REST point-of-access
 */
public interface UserStoreRestClient {

    /**
     * Retrieve the specified user form the REST server
     *
     * @param userID    id of the user
     * @param authToken auth token for the specified user (confidential)
     * @return retrieved user info
     * @throws ServiceUnavailableException Thrown if one of the components of the back-end is unavailable
     * @throws AuthException               Thrown if the specified userID and authToken are not valid or consistent
     * @throws NoUserException             Thrown if the specified user is not in the system
     */
    User getUserByID(String userID, String authToken) throws AuthException, NoUserException, ServiceUnavailableException;

}
