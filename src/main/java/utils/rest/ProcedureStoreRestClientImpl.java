package utils.rest;

import business.messages.ServicePageRetrievalMessage;
import business.pojo.Procedure;
import com.google.common.reflect.TypeToken;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.eclipse.jetty.http.HttpStatus;
import utils.config.Configurator;
import utils.customexceptions.AuthException;
import utils.customexceptions.NoProcedureException;
import utils.customexceptions.ServiceUnavailableException;
import utils.serialization.Serializer;

import java.lang.reflect.Type;
import java.util.List;

/**
 * Implementation of the procedure store Rest interface
 */
public class ProcedureStoreRestClientImpl implements ProcedureStoreRestClient {

    /**
     * Configurations specs
     */
    private static final String CONFIGURATION_NAMESPACE = "Rest";
    private static final String ADDRESS_CONFIG = "address_procedures";
    private static final String PORT_CONFIG = "port_procedures";
    private static final String PATH_CONFIG = "procedure_path";

    /**
     * response accepted format
     */
    private static final String ACCEPTED_HEADER_FIELD = "accept";
    private static final String ACCEPTED_FORMAT = "application/json";


    /**
     * Auth field in the HTTP header
     */
    private static final String AUTHORIZATION_HEADER_FIELD = "Authorization";


    private static final String PATH_SEPARATOR = "/";


    /**
     * Server specs
     */
    private String serverAddress;
    private int serverPort;

    /**
     * Paths in the procedure service
     */
    private static final String SERVICE_ADDRESS = "http://%s:%d";
    private static final String CREATE_PROCEDURE_PATH = "/api/procedure/%s";
    private static final String SINGLE_PROCEDURE_PATH = "/api/procedure/%s/%s";
    private static final String MULTIPLE_PROCEDURE_PATH = "/api/procedures/%s/%d/%d";


    /**
     * object serializer
     */
    private Serializer serializer;

    /**
     * Constructor
     * @param configurator configuration wrapper
     * @param serializer object serializer
     */
    public ProcedureStoreRestClientImpl(Configurator configurator, Serializer serializer) {
        this.serializer = serializer;
        this.getConfigurations(configurator);
    }

    /**
     * Get the configs from the wrapper
     */
    private void getConfigurations(Configurator configurations) {
        serverAddress = configurations.getConfiguration(CONFIGURATION_NAMESPACE, ADDRESS_CONFIG);
        serverPort = Integer.parseInt(configurations.getConfiguration(CONFIGURATION_NAMESPACE, PORT_CONFIG));
    }

    @Override
    public void createProcedure(Procedure procedure, String authToken) throws ServiceUnavailableException, AuthException {

        String address = this.createBasicAddress() + String.format(CREATE_PROCEDURE_PATH, procedure.getOwner());

        HttpResponse<JsonNode> jsonResponse;

        try {
            jsonResponse = Unirest.post(address)
                    .header(ACCEPTED_HEADER_FIELD, ACCEPTED_FORMAT)
                    .header(AUTHORIZATION_HEADER_FIELD, authToken)
                    .body(serializer.serialize(procedure))
                    .asJson();
        } catch (UnirestException e) {
            throw new ServiceUnavailableException();
        }

        switch (jsonResponse.getStatus()) {

            case HttpStatus.CREATED_201:
                // ok, do nothing
                break;
            // remote exeption handling
            case HttpStatus.UNAUTHORIZED_401:
                throw new AuthException();
            default:
                throw new ServiceUnavailableException();
        }

    }

    @Override
    public void updateProcedure(Procedure procedure, String authToken) throws ServiceUnavailableException, AuthException {

        String address = this.createBasicAddress() + String.format(SINGLE_PROCEDURE_PATH, procedure.getOwner(), procedure.getId());

        HttpResponse<JsonNode> jsonResponse;
        try {
            jsonResponse = Unirest.put(address)
                    .header(ACCEPTED_HEADER_FIELD, ACCEPTED_FORMAT)
                    .header(AUTHORIZATION_HEADER_FIELD, authToken)
                    .body(serializer.serialize(procedure))
                    .asJson();
        } catch (UnirestException e) {
            throw new ServiceUnavailableException();
        }

        switch (jsonResponse.getStatus()) {

            case HttpStatus.NO_CONTENT_204:
                // ok, do nothing
                break;
            // remote exeption handling
            case HttpStatus.UNAUTHORIZED_401:
                throw new AuthException();
            default:
                throw new ServiceUnavailableException();
        }
    }

    @Override
    public Procedure getProcedure(String ownerID, String procedureID, String authToken) throws NoProcedureException, ServiceUnavailableException, AuthException {

        String address = this.createBasicAddress() + String.format(SINGLE_PROCEDURE_PATH, ownerID, procedureID);

        HttpResponse<JsonNode> jsonResponse;
        Procedure retrieved;

        try {
            jsonResponse = Unirest.get(address)
                    .header(ACCEPTED_HEADER_FIELD, ACCEPTED_FORMAT)
                    .header(AUTHORIZATION_HEADER_FIELD, authToken)
                    .asJson();
        } catch (UnirestException e) {
            throw new ServiceUnavailableException();
        }

        switch (jsonResponse.getStatus()) {

            // user has been found
            case HttpStatus.OK_200:
                retrieved = (Procedure) serializer.deserialize(jsonResponse.getBody().toString(), Procedure.class);
                break;

            // remote exeption handling
            case HttpStatus.UNAUTHORIZED_401:
                throw new AuthException();
            case HttpStatus.NOT_FOUND_404:
                throw new NoProcedureException();
            default:
                throw new ServiceUnavailableException();
        }


        return retrieved;
    }

    @Override
    public void deleteProcedure(String ownerID, String procedureID, String authToken) throws ServiceUnavailableException, AuthException {

        String address = this.createBasicAddress() + String.format(SINGLE_PROCEDURE_PATH, ownerID, procedureID);

        HttpResponse<JsonNode> jsonResponse;
        try {
            jsonResponse = Unirest.delete(address)
                    .header(ACCEPTED_HEADER_FIELD, ACCEPTED_FORMAT)
                    .header(AUTHORIZATION_HEADER_FIELD, authToken)
                    .asJson();
        } catch (UnirestException e) {
            throw new ServiceUnavailableException();
        }

        switch (jsonResponse.getStatus()) {

            case HttpStatus.NO_CONTENT_204:
                // ok, do nothing
                break;
            // remote exeption handling
            case HttpStatus.UNAUTHORIZED_401:
                throw new AuthException();
            default:
                throw new ServiceUnavailableException();
        }

    }

    @Override
    public List<ServicePageRetrievalMessage> getPageProcedures(String ownerID, int pageNumber, int pageRange, String authToken) throws ServiceUnavailableException, AuthException {

        List<ServicePageRetrievalMessage> retrieved;
        String address = this.createBasicAddress() + String.format(MULTIPLE_PROCEDURE_PATH, ownerID, pageNumber, pageRange);

        Type listType = new TypeToken<List<ServicePageRetrievalMessage>>() {
        }.getType();

        HttpResponse<JsonNode> jsonResponse;
        try {
            jsonResponse = Unirest.get(address)
                    .header(ACCEPTED_HEADER_FIELD, ACCEPTED_FORMAT)
                    .header(AUTHORIZATION_HEADER_FIELD, authToken)
                    .asJson();
        } catch (UnirestException e) {
            throw new ServiceUnavailableException();
        }


        switch (jsonResponse.getStatus()) {

            // retrieved
            case HttpStatus.OK_200:
                // !!! cannot fix this genericity lose warning
                retrieved = (List<ServicePageRetrievalMessage>) serializer.deserialize(jsonResponse.getBody().toString(), listType);
                break;

            // remote exeption handling
            case HttpStatus.UNAUTHORIZED_401:
                throw new AuthException();
            default:
                throw new ServiceUnavailableException();
        }

        return retrieved;


    }


    /**
     * Retrieve the service address
     * @return service address
     */
    private String createBasicAddress() {
        return String.format(SERVICE_ADDRESS, serverAddress, serverPort);
    }
}
