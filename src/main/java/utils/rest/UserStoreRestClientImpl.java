package utils.rest;

import business.pojo.User;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.eclipse.jetty.http.HttpStatus;
import utils.config.Configurator;
import utils.customexceptions.AuthException;
import utils.customexceptions.NoUserException;
import utils.customexceptions.ServiceUnavailableException;
import utils.serialization.Serializer;

/**
 * Implementation of the user store Rest interface
 */
public class UserStoreRestClientImpl implements UserStoreRestClient {

    /**
     * Configurations specs
     */
    private static final String CONFIGURATION_NAMESPACE = "Rest";
    private static final String ADDRESS_CONFIG = "address_users";
    private static final String PORT_CONFIG = "port_users";
    private static final String PATH_CONFIG = "user_path";


    /**
     * response accepted format
     */
    private static final String ACCEPTED_HEADER_FIELD = "accept";
    private static final String ACCEPTED_FORMAT = "application/json";

    /**
     * Auth field in the HTTP header
     */
    private static final String AUTHORIZATION_HEADER_FIELD = "Authorization";


    /**
     * Server specs
     */
    private String serverAddress;
    private int serverPort;

    /**
     * Path in the default server
     */
    private static final String SERVICE_ADDRESS = "http://%s:%d";
    private static final String SINGLE_PROCEDURE_PATH = "/api/user/%s";

    private Serializer serializer;

    /**
     * Constructor
     *
     * @param configurator configuration wrapper
     * @param serializer   object serializer
     */
    public UserStoreRestClientImpl(Configurator configurator, Serializer serializer) {

        this.serializer = serializer;

        this.getConfigurations(configurator);
    }

    /**
     * Get the configs from the wrapper
     */
    private void getConfigurations(Configurator configurations) {
        serverAddress = configurations.getConfiguration(CONFIGURATION_NAMESPACE, ADDRESS_CONFIG);
        serverPort = Integer.parseInt(configurations.getConfiguration(CONFIGURATION_NAMESPACE, PORT_CONFIG));
    }


    @Override
    public User getUserByID(String userID, String authToken) throws AuthException, NoUserException, ServiceUnavailableException {

        String address = this.createBasicAddress() + String.format(SINGLE_PROCEDURE_PATH, userID);

        HttpResponse<JsonNode> jsonResponse = null;
        User retrieved;

        try {
            jsonResponse = Unirest.get(address)
                    .header(ACCEPTED_HEADER_FIELD, ACCEPTED_FORMAT)
                    .header(AUTHORIZATION_HEADER_FIELD, authToken)
                    .asJson();
        } catch (UnirestException e) {

            throw new ServiceUnavailableException();
        }

        switch (jsonResponse.getStatus()) {

            // user has been found
            case HttpStatus.OK_200:
                retrieved = (User) serializer.deserialize(jsonResponse.getBody().toString(), User.class);
                break;

            // remote exeption handling
            case HttpStatus.UNAUTHORIZED_401:
                throw new AuthException();
            case HttpStatus.NOT_FOUND_404:
                throw new NoUserException();
            default:
                throw new ServiceUnavailableException();
        }


        return retrieved;
    }

    /**
     * Retrieve the service address
     * @return service address
     */
    private String createBasicAddress() {
        return String.format(SERVICE_ADDRESS, serverAddress, serverPort);
    }
}
