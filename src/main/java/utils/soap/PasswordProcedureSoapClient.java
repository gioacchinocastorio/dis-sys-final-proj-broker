package utils.soap;

import business.pojo.Password;
import business.pojo.Procedure;
import business.pojo.User;
import utils.config.Configurator;
import utils.customexceptions.ServiceUnavailableException;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * SOAP password handling procedure point-of-access.
 * See the interface for more details
 */
public class PasswordProcedureSoapClient implements RemoteElaboratorSOAPClient {

    /**
     * Configurations specs
     */
    private static final String CONFIGURATION_NAMESPACE = "SOAP";
    private static final String WSD_ADDRESS = "wsdl";
    private static final String NAMESPACE = "service_namespace";
    private static final String SERVICE_NAME = "service_name";


    /**
     * Service specs
     */
    private String wsdlAddress;
    private String serviceNamespace;
    private String serviceName;

    /**
     * Actual remote interface
     */
    private PasswordGenerationServer remoteElaborator;

    /**
     * Constructor
     *
     * @param configurator configuration wrapper
     */
    public PasswordProcedureSoapClient(Configurator configurator) {
        this.getConfigurations(configurator);
        this.connect();
    }

    /**
     * Connect to the remote remoteElaborator
     */
    private void connect() {

        URL wsdlUrl = null;
        QName qname = null;
        Service service = null;

        try {
            wsdlUrl = new URL(wsdlAddress);
        } catch (MalformedURLException e) {
            System.err.println("ERROR: the WSDL for the SOAP remoteElaborator is not properly written in the configs");
            System.exit(1);
        }

        try {
            // connect to the remote remoteElaborator provider
            qname = new QName(serviceNamespace, serviceName);
            service = Service.create(wsdlUrl, qname);
        } catch (Exception e) {
            System.err.println("ERROR: the remoteElaborator is not unreachable!!!");
            System.exit(1);

        }

        // retrieve the remoteElaborator
        remoteElaborator = service.getPort(PasswordGenerationServer.class);


    }

    /**
     * Get the configs from the wrapper
     */
    private void getConfigurations(Configurator configurations) {
        wsdlAddress = configurations.getConfiguration(CONFIGURATION_NAMESPACE, WSD_ADDRESS);
        serviceNamespace = configurations.getConfiguration(CONFIGURATION_NAMESPACE, NAMESPACE);
        serviceName = configurations.getConfiguration(CONFIGURATION_NAMESPACE, SERVICE_NAME);
    }

    @Override
    public Procedure getProcedure(String service, User userInfo) throws ServiceUnavailableException {

        Procedure retrieved;

        try {
            retrieved = remoteElaborator.getProcedure(service, userInfo);
        } catch (Exception e) {
            System.err.println("ERROR: the remote system seems not responding");
            throw new ServiceUnavailableException();
        }

        return retrieved;
    }

    @Override
    public Password getPassword(Procedure procedure) throws ServiceUnavailableException {

        Password retrieved;

        try {
            retrieved = remoteElaborator.getPassword(procedure);
        } catch (Exception e) {
            System.err.println("ERROR: the remote system seems not responding");
            throw new ServiceUnavailableException();
        }

        return retrieved;
    }
}
