package utils.soap;

import business.pojo.Password;
import business.pojo.Procedure;
import business.pojo.User;
import utils.customexceptions.ServiceUnavailableException;

/**
 * SOAP password handling procedure point-of-access.
 * This is an adapter interface.
 */
public interface RemoteElaboratorSOAPClient {

    /**
     * Retrieve a procedure form the remote server
     *
     * @param service  name of the service for the new procedure
     * @param userInfo password owner's information
     * @return generated procedure
     * @throws ServiceUnavailableException thrown if the services is not available
     */
    Procedure getProcedure(String service, User userInfo) throws ServiceUnavailableException;

    /**
     * Decrypt a procedure and receive a password from the remote server
     *
     * @param procedure encryped procedure
     * @return retrieved password
     * @throws ServiceUnavailableException thrown if the services is not available
     */
    Password getPassword(Procedure procedure) throws ServiceUnavailableException;

}
