package utils.customexceptions;

/**
 * Remote server unavailable exception
 */
public class ServiceUnavailableException extends Exception {
}
