package utils.customexceptions;

/**
 * Procedure (password generation) not found exception
 */
public class NoProcedureException extends Exception {
}
