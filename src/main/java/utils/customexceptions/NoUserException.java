package utils.customexceptions;

/**
 * Impossible to retrieve the user exception
 */
public class NoUserException extends Exception {
}
