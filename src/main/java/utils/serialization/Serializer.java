package utils.serialization;

import java.lang.reflect.Type;

public interface Serializer {

    /**
     * Serialize an generic object
     * @param object Object to serialize
     * @return Serialized string
     */
    String serialize(Object object);

    /**
     * Demarshaling of the specified string
     * @param serializedString serialized object
     * @param prototype the string will be casted according to this prototype
     * @return deserialized object
     */
    Object deserialize(String serializedString, Type prototype);


}
