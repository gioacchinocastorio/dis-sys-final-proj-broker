package utils.serialization;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;

/**
 * This class is just an adapter for Gson
 */
public class GsonSerializerImpl implements Serializer {

    /**
     * Actual serializer
     */
    private final Gson marshaller;

    public GsonSerializerImpl() {

        marshaller = new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .setPrettyPrinting() // well formatted jsons (tabs, carriage returns, etc.)
                .create();
    }

    @Override
    public String serialize(Object object) {
        return marshaller.toJson(object);
    }

    @Override
    public Object deserialize(String serializedString, Type prototype) {

        String serialized;

        // in order to check if the JSON is well formed
        try {
            // try as normal json
            serialized = new JSONObject(serializedString).toString();

        } catch (JSONException jsonMalformed) {
            // try as array
            serialized = new JSONArray(serializedString).toString();
        }

        return marshaller.fromJson(serialized, prototype);
    }

}
