package controllers;

import business.RequestHandler;
import business.messages.AuthParameters;
import business.messages.ErrorMessage;
import business.messages.PasswordCreationMessage;
import business.pojo.Password;
import org.eclipse.jetty.http.HttpMethod;
import org.eclipse.jetty.http.HttpStatus;
import org.json.JSONException;
import org.json.JSONObject;
import spark.*;
import utils.config.Configurator;
import utils.customexceptions.AuthException;
import utils.customexceptions.NoProcedureException;
import utils.customexceptions.NoUserException;
import utils.customexceptions.ServiceUnavailableException;
import utils.requestintegritycheck.IntegrityChecker;
import utils.serialization.Serializer;

import java.util.Objects;

/**
 * JavaSpark REST controller.
 */
public class SparkRestControllerImpl implements Controller {

    /**
     * Configuration information in the config.ini file
     */
    private static final String CONFIGURATION_NAMESPACE = "Spark";
    private static final String PORT_CONFIG = "port";


    /**
     * Default content of and empty response
     */
    private static final String EMPTY_RESPONSE = "";


    /**
     * The web server will listen here
     */
    private int listeningPort = 0;

    /**
     * message format
     */
    private static final String COMMUNICATION_FORMAT = "application/json";

    /**
     * In a route, this is the parameter that contains the userID
     */
    private static final String USER_ID_PARAMETER = ":user";

    /**
     * In a route, this is the parameter that contains the procedure ID
     */
    private static final String PASSWORD_ID_PARAMETER = ":password";

    /**
     * In a route, this is the parameter that contains the page ID
     */
    private static final String PAGE_NUMBER_PARAMETER = ":page";

    /**
     * In a route, this is the parameter that contains the offset of results in page ID
     */
    private static final String RANGE_IN_PAGE_PARAMETER = ":range";

    /**
     * Request header for authentication (contains the token)
     */
    private static final String TOKEN_HEADER = "Authorization";

    /**
     * Route components
     */
    private static final String ROUTE_WILDCARD = "*";
    private static final String SAME_PATH_ROUTE = "";


    /**
     * Model serializer (object to JSON)
     */
    private Serializer serializer;

    /**
     * Configuration wrapper
     */
    private Configurator configurations;

    /**
     * JSON integrity checker
     */
    private IntegrityChecker integrityChecker;

    /**
     * Business logic handler
     */
    private RequestHandler requestHandler;


    /**
     * Constructor
     *
     * @param serializer     Model serializer (object to JSON)
     * @param configurations Configuration wrapper
     * @param requestHandler Business logic handler
     * @param checker        JSON integrity checker
     */
    public SparkRestControllerImpl(Serializer serializer,
                                   Configurator configurations,
                                   RequestHandler requestHandler,
                                   IntegrityChecker checker) {
        this.serializer = serializer;
        this.configurations = configurations;
        this.requestHandler = requestHandler;
        this.integrityChecker = checker;

        this.getConfigurations();
    }

    @Override
    public void startListening() {
        this.configureListeningServer();
        this.setupRoutes();
        this.waitServerSetup();

    }

    /**
     * Start the server conf
     */
    private void configureListeningServer() {

        getConfigurations();

        // listening port
        try {
            Spark.port(listeningPort);
        } catch (Exception e) {
            System.err.println("ERROR: the selected port is occupied by another service");
            System.exit(1);
        }

    }

    /**
     * Get the configs from the wrapper
     */
    private void getConfigurations() {
        listeningPort = Integer.parseInt(configurations.getConfiguration(CONFIGURATION_NAMESPACE, PORT_CONFIG));
    }

    /**
     * Activate the routes on the server
     */
    private void setupRoutes() {

        // COORS allowance
        enableCORS(ROUTE_WILDCARD, "GET, POST, PUT, DELETE", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,");


        // user auth checks
        Spark.before(ROUTE_WILDCARD, COMMUNICATION_FORMAT, new LogFilter(), new CheckJSONIntegrityFilter());


        // routes
        Spark.path("/api", () -> {

                    // routes for operations on a specific password  /password/...
                    Spark.path("/password/:user", () -> {

                        Spark.before(ROUTE_WILDCARD, COMMUNICATION_FORMAT, new RequestPermissionFilter());

                        // password creation
                        Spark.post(SAME_PATH_ROUTE, COMMUNICATION_FORMAT, new PasswordCreationRoute());
                        // password retrieval
                        Spark.get("/:password", COMMUNICATION_FORMAT, new PasswordRetrievalRoute(), serializer::serialize);
                        // password deletion
                        Spark.delete("/:password", COMMUNICATION_FORMAT, new PasswordDeletionRoute());

                    });

                    // routes for operations on password groups  /password/...
                    Spark.path("/passwords/:user", () -> {

                        Spark.before(ROUTE_WILDCARD, COMMUNICATION_FORMAT, new RequestPermissionFilter());

                        // batch retrieval of the passwords
                        Spark.get("/:page/:range", COMMUNICATION_FORMAT, new PasswordPageRetrieval(), serializer::serialize);

                    });


                }
        );

        // system error handling
        Spark.internalServerError(new InternalErrorRoute()); // server fault
        Spark.notFound(new ResourceNotFoundRoute()); // resource not found

        // set all the responses as JSON
        Spark.after(((request, response) -> response.header("content-type", COMMUNICATION_FORMAT)));


    }


    /**
     * Wait for the server to set up
     */
    private void waitServerSetup() {

        // this blocks the thread
        Spark.awaitInitialization(); // Wait for server to be initialized

    }

    /**
     * Creation of response for badly structured requests
     *
     * @param response http response wrapper
     * @return error message
     */
    private ErrorMessage setMalformedRequest(Response response) {
        ErrorMessage responseContent;
        response.status(HttpStatus.UNPROCESSABLE_ENTITY_422);
        responseContent = ErrorMessage.getMalformedRequest();
        return responseContent;
    }


    /**
     * Check if the user is authorized
     */
    private class RequestPermissionFilter implements Filter {

        @Override
        public void handle(Request request, Response response) throws HaltException {

            if (!Objects.equals(request.requestMethod(), HttpMethod.OPTIONS.asString())) {
                String userid = request.params(USER_ID_PARAMETER);
                String authToken = request.headers(TOKEN_HEADER);
                AuthParameters authParameters = new AuthParameters(authToken, userid);

                // if the user is not authorized, throw error
                if (!requestHandler.isRequestAuthorised(authParameters)) {
                    Spark.halt(HttpStatus.UNAUTHORIZED_401);
                }
            }


        }
    }


    /**
     * Route for 500 error
     */
    private class InternalErrorRoute implements Route {

        @Override
        public Object handle(Request request, Response response) throws Exception {
            return serializer.serialize(ErrorMessage.getInternalError());
        }
    }

    /**
     * Route for 404 error
     */
    private class ResourceNotFoundRoute implements Route {

        @Override
        public Object handle(Request request, Response response) throws Exception {
            return serializer.serialize(ErrorMessage.getNotFoundError());
        }
    }

    /**
     * Checks if the request is is proper JSON
     */
    private class CheckJSONIntegrityFilter implements Filter {
        @Override
        public void handle(Request request, Response response) throws Exception {

            // if there is a data input request
            if (Objects.equals(request.requestMethod(), HttpMethod.POST.asString()) || Objects.equals(request.requestMethod(), HttpMethod.PUT.asString())) {

                String json = request.body();

                // check if the request is valid json
                try {
                    new JSONObject(json);
                } catch (JSONException e) {
                    Spark.halt(HttpStatus.BAD_REQUEST_400);
                }
            }
        }
    }

    /**
     * Route for password creation
     */
    private class PasswordCreationRoute implements Route {
        @Override
        public Object handle(Request request, Response response) throws Exception {

            Object responseContent;
            String jsonProcedure = request.body();

            // user id of the issuer
            String owner = request.params(USER_ID_PARAMETER);
            // auth token of the issuer
            String authToken = request.headers(TOKEN_HEADER);


            // check if the request complies to the data format specification
            if (integrityChecker.isPasswordCreationRequestWellFormed(jsonProcedure)) {

                PasswordCreationMessage newPasswordParametes =
                        (PasswordCreationMessage) serializer.deserialize(
                                jsonProcedure,
                                PasswordCreationMessage.class);

                // password creation
                try {
                    requestHandler.createPassword(owner, authToken, newPasswordParametes.getService());
                } catch (ServiceUnavailableException e) {
                    System.err.println("ERROR: the remote Procedure Store system seems not responding");
                    Spark.halt(HttpStatus.INTERNAL_SERVER_ERROR_500);
                } catch (AuthException e) {
                    Spark.halt(HttpStatus.UNAUTHORIZED_401);
                } catch (NoUserException e) {
                    Spark.halt(HttpStatus.BAD_REQUEST_400);
                }


                response.status(HttpStatus.CREATED_201);
                responseContent = EMPTY_RESPONSE;

            } else {

                // malformed request
                responseContent = serializer.serialize(setMalformedRequest(response));

            }

            return responseContent;
        }
    }


    /**
     * Bulk retrieval of the user's passwords (paged)
     */
    private class PasswordPageRetrieval implements Route {
        @Override
        public Object handle(Request request, Response response) throws Exception {

            Object responseContent = null;
            String authToken = "";
            String owner = "";
            int pageNumber = 0;
            int pageRange = 0;

            try {

                // retrieve the information from the route
                owner = request.params(USER_ID_PARAMETER);
                authToken = request.headers(TOKEN_HEADER);
                pageNumber = Integer.parseInt(request.params(PAGE_NUMBER_PARAMETER));
                pageRange = Integer.parseInt(request.params(RANGE_IN_PAGE_PARAMETER));

            } catch (NumberFormatException e) {
                // the user sent irregular infos for page and range
                Spark.halt(HttpStatus.BAD_REQUEST_400);

            }

            // get a page
            try {
                responseContent = requestHandler.getPagedServices(owner, authToken, pageNumber, pageRange);
            } catch (AuthException e) {
                Spark.halt(HttpStatus.UNAUTHORIZED_401);
            } catch (ServiceUnavailableException e) {
                System.err.println("ERROR: the remote Procedure Store system seems not responding");
                Spark.halt(HttpStatus.INTERNAL_SERVER_ERROR_500);
            }

            return responseContent;
        }
    }


    /**
     * Retrieve a specified password
     */
    private class PasswordRetrievalRoute implements Route {
        @Override
        public Object handle(Request request, Response response) throws Exception {

            String passwordID = request.params(PASSWORD_ID_PARAMETER);
            String owner = request.params(USER_ID_PARAMETER);
            String authToken = request.headers(TOKEN_HEADER);

            Password retrieved = null;

            // retrieve the password
            try {

                retrieved = requestHandler.getPassword(passwordID, owner, authToken);

                // if there is no content, show not found
                if (retrieved == null) {
                    Spark.halt(HttpStatus.NOT_FOUND_404);
                }

            } catch (ServiceUnavailableException e) {
                System.err.println("ERROR: the remote Procedure Store system seems not responding");
                Spark.halt(HttpStatus.INTERNAL_SERVER_ERROR_500);
            } catch (AuthException e) {
                Spark.halt(HttpStatus.UNAUTHORIZED_401);
            } catch (NoProcedureException e) {
                Spark.halt(HttpStatus.NOT_FOUND_404);
            }

            return retrieved;
        }
    }

    /**
     * Password deletion procedure
     */
    private class PasswordDeletionRoute implements Route {
        @Override
        public Object handle(Request request, Response response) throws Exception {

            String passwordID = request.params(PASSWORD_ID_PARAMETER);
            String owner = request.params(USER_ID_PARAMETER);
            String authToken = request.headers(TOKEN_HEADER);


            try {
                requestHandler.deletePassword(passwordID, owner, authToken);
                response.status(HttpStatus.NO_CONTENT_204);
            } catch (AuthException e) {
                Spark.halt(HttpStatus.UNAUTHORIZED_401);
            } catch (ServiceUnavailableException e) {
                System.err.println("ERROR: the remote Procedure Store system seems not responding");
                Spark.halt(HttpStatus.INTERNAL_SERVER_ERROR_500);
            }


            return EMPTY_RESPONSE;
        }
    }


    // https://sparktutorials.github.io/2016/05/01/cors.html
    //Enables CORS on requests. This method is an initialization method and should be called once.
    private static void enableCORS(final String origin, final String methods, final String headers) {

        Spark.options("/*", (request, response) -> {


            String accessControlRequestHeaders = request.headers("Access-Control-Request-Headers");
            if (accessControlRequestHeaders != null) {
                response.header("Access-Control-Allow-Headers", accessControlRequestHeaders);
            }

            String accessControlRequestMethod = request.headers("Access-Control-Request-Method");
            if (accessControlRequestMethod != null) {
                response.header("Access-Control-Allow-Methods", accessControlRequestMethod);
            }

            String accessControlRequestCredential = request.headers("Access-Control-Allow-Credentials");
            if (accessControlRequestCredential != null) {
                response.header("Access-Control-Allow-Credentials", "true");
            }

            return "OK";
        });

        Spark.before((request, response) -> {
            response.header("Access-Control-Allow-Origin", origin);
            response.header("Access-Control-Request-Method", methods);
            response.header("Access-Control-Allow-Headers", headers);
            response.header("Access-Control-Allow-Credentials", "true");
            // Note: this may or may not be necessary in your particular application
            response.type("application/json");
        });
    }


    /**
     * Log the incoming requests
     */
    private class LogFilter implements Filter {
        @Override
        public void handle(Request request, Response response) throws Exception {

            System.out.println("Received request");
            System.out.println("Method: " + request.requestMethod());
            System.out.println("Client address: " + request.ip());
            System.out.println("Route: " + request.pathInfo());
            System.out.println("Content: " + request.body());
            System.out.println("\n\n");


        }
    }
}