package business;

import business.messages.AuthParameters;
import business.messages.ServicePageRetrievalMessage;
import business.pojo.Password;
import utils.customexceptions.AuthException;
import utils.customexceptions.NoProcedureException;
import utils.customexceptions.NoUserException;
import utils.customexceptions.ServiceUnavailableException;

import java.util.List;

/**
 * This entity hides the business logic of the system.
 */
public interface RequestHandler {

    /**
     * Password creation procedure
     *
     * @param owner     ID in the system of the issuer (provided by the User Store)
     * @param authToken JWT of the issuer (provided by the User Store)
     * @param service   Label of the service associated to the password
     * @throws ServiceUnavailableException Thrown if one of the components of the back-end is unavailable
     * @throws AuthException               Thrown if the specified userID and authToken are not valid or consistent
     * @throws NoUserException             Thrown if the specified user is not in the system
     */
    void createPassword(String owner, String authToken, String service) throws ServiceUnavailableException, AuthException, NoUserException;

    /**
     * Password retrieval procedure
     *
     * @param passwordID ID of the password in the system
     * @param userID     ID in the system of the issuer (provided by the User Store)
     * @param authToken  JWT of the issuer (provided by the User Store)
     * @return Decrypted password
     * @throws ServiceUnavailableException Thrown if one of the components of the back-end is unavailable
     * @throws AuthException               Thrown if the specified userID and authToken are not valid or consistent
     * @throws NoProcedureException        Thrown if the specified passwordID is not valid
     */
    Password getPassword(String passwordID, String userID, String authToken) throws ServiceUnavailableException, AuthException, NoProcedureException;

    /**
     * Delete a password in the system
     *
     * @param passwordID ID of the password in the system
     * @param owner      ID in the system of the issuer (provided by the User Store)
     * @param authToken  JWT of the issuer (provided by the User Store)
     * @throws ServiceUnavailableException Thrown if one of the components of the back-end is unavailable
     * @throws AuthException               Thrown if the specified userID and authToken are not valid or consistent
     */
    void deletePassword(String passwordID, String owner, String authToken) throws AuthException, ServiceUnavailableException;

    /**
     * Get list of password info (the retrieval system is paged)
     *
     * @param owner      ID in the system of the issuer (provided by the User Store)
     * @param authToken  JWT of the issuer (provided by the User Store)
     * @param pageNumber Number of the requested page
     * @param pageRange  Number of results in the page (if it exceeds the page size, it returns the maximum value)
     * @return Password info
     * @throws ServiceUnavailableException Thrown if one of the components of the back-end is unavailable
     * @throws AuthException               Thrown if the specified userID and authToken are not valid or consistent
     */
    List<ServicePageRetrievalMessage> getPagedServices(String owner, String authToken, int pageNumber, int pageRange) throws AuthException, ServiceUnavailableException;

    /**
     * Check if the request is authorized according to this server
     * @param parameters Authentication parameters object
     * @return true if the request is authorized
     */
    boolean isRequestAuthorised(AuthParameters parameters);

}
