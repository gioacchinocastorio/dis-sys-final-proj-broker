package business.messages;

import business.pojo.Password;
import com.google.gson.annotations.Expose;

/**
 * This class represents the content of the answer when the user asks for his password info
 */
public class ServicePageRetrievalMessage {

    /**
     * ID of the password in the system
     */
    @Expose private String id;

    /**
     * Label of the service associated to the password
     */
    @Expose private String serviceName;

    public ServicePageRetrievalMessage(Password procedure) {
        this.id = procedure.getId();
        this.serviceName = procedure.getServiceName();
    }

    public String getId() {
        return id;
    }

    public String getServiceName() {
        return serviceName;
    }

    @Override
    public String toString() {
        return "ServicePageRetrievalMessage{" +
                "id='" + id + '\'' +
                ", serviceName='" + serviceName + '\'' +
                '}';
    }
}
