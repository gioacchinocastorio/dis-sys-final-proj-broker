package business.messages;

import com.google.gson.annotations.Expose;

/**
 * This class represents the message received when the user wants to add a new password in the store
 */
public class PasswordCreationMessage {

    /**
     * Label of the service associated to the password
     */
    @Expose private String service;

    public String getService() {
        return service;
    }

    public void setService(String services) {
        this.service = services;
    }
}
