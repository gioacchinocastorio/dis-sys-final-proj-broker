package business;

import business.messages.AuthParameters;
import business.messages.ServicePageRetrievalMessage;
import business.pojo.Password;
import business.pojo.Procedure;
import business.pojo.User;
import utils.customexceptions.AuthException;
import utils.customexceptions.NoProcedureException;
import utils.customexceptions.NoUserException;
import utils.customexceptions.ServiceUnavailableException;
import utils.rest.ProcedureStoreRestClient;
import utils.rest.UserStoreRestClient;
import utils.soap.RemoteElaboratorSOAPClient;
import utils.token.TokenStrategy;

import java.util.List;

/**
 * Basic implementation fo the RequestHandler interface.
 * See the interface to get more details
 */
public class RequestHandlerImpl implements RequestHandler {

    /**
     * Auth token handler
     */
    private final TokenStrategy tokenStrategy;

    /**
     * Remote procedure store REST server
     */
    private final ProcedureStoreRestClient procedureStore;

    /**
     * Remote user store REST server
     */
    private final UserStoreRestClient userStore;

    /**
     * Remote password procedure handler SOAP server
     */
    private final RemoteElaboratorSOAPClient cryptoProcedure;


    /**
     * Constructor
     *
     * @param tokenStrategy   Auth token handler
     * @param procedureStore  Remote procedure store REST server
     * @param userStore       Remote user store REST server
     * @param cryptoProcedure Remote password procedure handler SOAP server
     */
    public RequestHandlerImpl(TokenStrategy tokenStrategy, ProcedureStoreRestClient procedureStore, UserStoreRestClient userStore, RemoteElaboratorSOAPClient cryptoProcedure) {
        this.tokenStrategy = tokenStrategy;
        this.procedureStore = procedureStore;
        this.userStore = userStore;
        this.cryptoProcedure = cryptoProcedure;
    }

    @Override
    public void createPassword(String userID, String authToken, String service) throws ServiceUnavailableException, AuthException, NoUserException {

        // retrieve the owner by id
        User owner = userStore.getUserByID(userID, authToken);

        // create the procedure using the info
        Procedure newProcedure = cryptoProcedure.getProcedure(service, owner);

        // store the info in the
        procedureStore.createProcedure(newProcedure, authToken);


    }

    private void updatePassword(String passwordID, String userID, String authToken, String newPin) throws ServiceUnavailableException, AuthException, NoProcedureException, NoUserException {

        // retrieve the old procedure
        Procedure oldProcedure = procedureStore.getProcedure(userID, passwordID, authToken);

        // retrieve the owner by id
        User owner = userStore.getUserByID(userID, authToken);

        // create a new procedure
        Procedure newProcedure = cryptoProcedure.getProcedure(oldProcedure.getServiceName(), owner);

        // set the id to the modified procedure
        newProcedure.setId(oldProcedure.getId());

        // update the procedure in the store
        procedureStore.updateProcedure(newProcedure, authToken);

    }

    @Override
    public Password getPassword(String passwordID, String userID, String authToken) throws ServiceUnavailableException, AuthException, NoProcedureException {

        // retrieve the  procedure
        Procedure procedure = procedureStore.getProcedure(userID, passwordID, authToken);

        return cryptoProcedure.getPassword(procedure);
    }

    @Override
    public void deletePassword(String passwordID, String owner, String authToken) throws AuthException, ServiceUnavailableException {
        procedureStore.deleteProcedure(owner, passwordID, authToken);
    }

    @Override
    public List<ServicePageRetrievalMessage> getPagedServices(String owner, String authToken, int pageNumber, int pageRange) throws AuthException, ServiceUnavailableException {

        return procedureStore.getPageProcedures(owner, pageNumber, pageRange, authToken);

    }

    @Override
    public boolean isRequestAuthorised(AuthParameters parameters) {
        return this.tokenStrategy.isTokenValid(parameters.getAuthToken(), parameters.getUserId());
    }

}
