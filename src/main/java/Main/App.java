package Main;

import business.RequestHandler;
import business.RequestHandlerImpl;
import controllers.Controller;
import controllers.SparkRestControllerImpl;
import utils.config.Configurator;
import utils.config.ConfiguratorIniImpl;
import utils.requestintegritycheck.IntegrityChecker;
import utils.requestintegritycheck.JSONSchemaDraft6IntegrityCheckerImpl;
import utils.rest.ProcedureStoreRestClient;
import utils.rest.ProcedureStoreRestClientImpl;
import utils.rest.UserStoreRestClient;
import utils.rest.UserStoreRestClientImpl;
import utils.serialization.GsonSerializerImpl;
import utils.serialization.Serializer;
import utils.soap.PasswordProcedureSoapClient;
import utils.soap.RemoteElaboratorSOAPClient;
import utils.token.JWTTokenStrategyImpl;
import utils.token.TokenStrategy;

import java.io.File;

/**
 * Application point of access
 */
public class App {

    public static void main(String[] args) {

        // configurations
        File configFile = new File("configs/config.ini");
        Configurator configurator = new ConfiguratorIniImpl(configFile);

        // utils
        TokenStrategy tokengen = new JWTTokenStrategyImpl(configurator);
        Serializer serializer = new GsonSerializerImpl();
        IntegrityChecker cheker = new JSONSchemaDraft6IntegrityCheckerImpl(configurator);

        // remote services
        UserStoreRestClient restClient = new UserStoreRestClientImpl(configurator, serializer);
        ProcedureStoreRestClient procedureStoreRestClient = new ProcedureStoreRestClientImpl(configurator, serializer);
        RemoteElaboratorSOAPClient soapClient = new PasswordProcedureSoapClient(configurator);


        // business logic
        RequestHandler requestHandler = new RequestHandlerImpl(tokengen, procedureStoreRestClient, restClient, soapClient);


        // start the web server
        Controller restBroker = new SparkRestControllerImpl(serializer, configurator, requestHandler, cheker);
        restBroker.startListening();

    }
}
